Eluvio - challenge workflow - Tim Cvetko

Here's what the challenge is comprised of. I wanted to create a model that will predict how good the article was based on all data. As a label I used the ratio of up_votes versus down_votes, which should be a good hallmark for article quality. For the NLP part, I used the BERT toxicity model, to correctly output values indicating harsh titles. I wanted to combine this with the author, since I noticed that we might come across similarties regarding titles and authors. For example: Palestinian wielding knife shot: by superislam. 



I loaded the data using Google Colab and Drive to a dataframe. I envisioned what needs to be done in pandas, but since the data was pretty big, I used pyspark pipeline to feed the model.

EDA 
	- dropped the category, since they all belonged in the same category
	- converted the date into how many days it was since that article was being published compared to recent date, normalized by taking the min-max normalization so the gradient doesn't necessarily blow up. 
	- for the time_created I substracted all dates from the largest value, so the new intuition is the smallest value, the largest time passed since then. 
	- I applied the binary encoder for the over_18 category, 1 meaning over 18, and 0 meaning under. 
	
We end up with 6 columns, non of which have any null values. I created a Spark pipeline which is then fed into Elephas estimator (keras deep learning framework supporting spark pipelines).

I've decided to take on a TensorFlow deep neural network model, comprised of 4 Dense layers, 2 batch normalization and 2 dropout layers. It has 10465 parameters. Three callbacks support the model training; early stopping, reduce learning rate on plateau and the model checkpoint that stores the best weights every epoch. 

Once the model is trained, hyperparameter tuning takes place. As a final comment, I have to say that the model wasn't trained on the full data. However, I built a sufficient Spark pipeline that can be easily applied on a cloud(AWS, GCP with Dataflow) with scalability. I don't currently posess a cloud account, otherwise I'd be more than happy to take it to scale. I hope this is not a major setback and I'm looking forward to your feedback. 
